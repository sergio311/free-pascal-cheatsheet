# Language

* Pascal is not Case Sensitive.
* 

## Identifiers

* They are composed of 1 and up to 127 significan characters.
* Significan characters: letters, numbers and underscore.
* Must start with a letter or an undcercore.

### Hint directives
* When the compiler finds them a warning notice is issued.
* Must be placed before the identifier
* Types:
    * deprecated: indicates that this identifier is deprecated, it can be include a text message.
    * experimental: indicates that this identifier is experimental.
    * platform: indicates that this identifier is platform dependent.
    * unimplemented: indicates that this identifier is not implemented, it only applies to functions and procedures.

## Literals
## Reserved Words
* Turbo Pascal reserved words:
absolute  and  array  asm  begin  case  const  constructor  destructor  div  do  downto  else  end  file  for  function  
goto  if  implementation  in  inherited  inline  interface  label  mod  nil  not  object  of  operator  or  packed  procedure  
program  record  reintroduce  repeat  self  set  shl  shr  string  then  to  type  unit  until  uses  var  while  with  xor

* Object Pascal reserved words:
as  class  dispinterface  except  exports  finalization  finally  initialization  inline  is  library  on  out  packed  property  
raise  resourcestring  threadvar  try

* The next words can be used as identifiers:
absolute  abstract  alias  assembler  bitpacked  break  cdecl  continue  cppdecl  cvar  default  deprecated  dynamic  enumerator  
experimental  export  external  far  far16  forward  generic  helper  implements  index  interrupt  iocheck  local  message  
name  near  nodefault  noreturn  nostackframe  oldfpccall  otherwise  overload  override  pascal  platform  private  protected  
public  published  read  register  reintroduce  result  safecall  saveregisters  softfloat  specialize  static  stdcall  stored  
strict  unaligned  unimplemented  varargs  virtual  winapi  write

## Comments

* One line:
```pascal
    //One line comment.
```
* Multiple line
```pascal
    (* 
        Old style multiple line comment 
    *)
    {
        New style multiple line comment
    }
```
